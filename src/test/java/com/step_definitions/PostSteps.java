package com.step_definitions;

import com.modules.PostAction;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PostSteps {

	@Given("^User add \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" to Json$")
	public void user_add_to_Json(String id, String title, String author) throws Throwable {
		PostAction.postData(id, title, author);
	}

	@When("^User send a POST request$")
	public void user_send_a_POST_request() throws Throwable {
		PostAction.postRequest();

	}

	@And("^the server should handle it and return a success status$")
	public void the_server_should_handle_it_and_return_a_success_status() throws Throwable {

		PostAction.postStatusCode();

	}
	
	@Then("^User should check data has posted or not$")
	public void user_should_check_data_has_posted_or_not() throws Throwable {
		PostAction.validatePostData();
	}
	
	//**********************************Negative Test Cases**********************************//	
	
	
	@Given("^User add duplicate \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" to Json$")
	public void user_add_duplicate_to_Json(String id, String title, String author) throws Throwable {
		PostAction.duplicateData(id, title, author);
	}

	@Then("^the server should return a invalid status code$")
	public void the_server_should_return_a_invalid_status_code() throws Throwable {
		PostAction.postNegStatusCode();
	}



}
