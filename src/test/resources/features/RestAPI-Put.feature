Feature: Testing a REST API 
	Users should be able to submit PUT request to a web service 


@put 
Scenario Outline: Data Modify in a web service 
	Given User wants to modify "<title>" "<author>" in Json 
	When User send a PUT request 
	And the server should return a success status 
	And User send a GET request
	Then User should check data has modified or not
	
Examples:
	
		 |  title    |  author     | 
		 |  ruby1    |  raji1      |	

@put-neg 
Scenario Outline: Testing put request by giving invalid URI 
	Given User wants to modify "<title>" "<author>" in Json 
	When User send a PUT request by giving invalid id 
	Then the server should return a invalid status 

Examples:
	
		 |  title    |  author     | 
		 |  ruby1    |  raji1      |